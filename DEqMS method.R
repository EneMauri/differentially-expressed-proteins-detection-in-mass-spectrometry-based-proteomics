#Based on http://127.0.0.1:26123/library/DEqMS/doc/DEqMS-package-vignette.R
## ----Loadpackage---------------------------------------------------------
library(DEqMS)
## ----UploadLabelfreeData from Parc Cientific ----------------------------
#LFQ intensities in “proteinGroups.txt” table,
## ----LFQprotein----------------------------------------------------------
df.prot= read.csv("Proteomics_ANOVA_Results_R1620.csv", header = T, sep = "\t", row.names = 1)
nrow(df.prot) #3345
# remove decoy matches and matches to contaminant (already done)
#df.prot = df.prot[!df.prot$Reverse=="+",]
#df.prot = df.prot[!df.prot$Contaminant=="+",]

# Extract columns of LFQ intensites
LFQ_columns <- grep("LFQ", colnames(df.prot)) # get LFQ column numbers
df.LFQ= read.csv("Proteomics_ANOVA_Results_R1620.csv", header = T, sep = "\t", row.names = 1)[,LFQ_columns]
df.LFQ= df.LFQ[,c(-2,-3)]
df.LFQ[df.LFQ==0] <- NA

#rownames(df.LFQ) = df.prot$Majority.protein.IDs
df.LFQ$na_count_a = apply(df.LFQ,1,function(x) sum(is.na(x[1:3])))
df.LFQ$na_count_b = apply(df.LFQ,1,function(x) sum(is.na(x[4:6])))
df.LFQ$na_count_c = apply(df.LFQ,1,function(x) sum(is.na(x[7:9])))
df.LFQ$na_count_d = apply(df.LFQ,1,function(x) sum(is.na(x[10:12])))

# Filter protein table. DEqMS require minimum two values for each group.
#df.LFQ.filter = df.LFQ[df.LFQ$na_count_a<2 & df.LFQ$na_count_b<2 & 
#                         df.LFQ$na_count_c<2 & df.LFQ$na_count_d<2,1:12]
#nrow(df.LFQ.filter)#1792

# Filter protein table. DEqMS require minimum three values at least one each group.
df.LFQ.filter= df.LFQ[(df.LFQ$na_count_a==0 | df.LFQ$na_count_a==3) & 
                        (df.LFQ$na_count_b==0 |df.LFQ$na_count_b==3) &
                        (df.LFQ$na_count_c==0 | df.LFQ$na_count_c==3) &
                        (df.LFQ$na_count_d==0 | df.LFQ$na_count_d==3) &
                        !(df.LFQ$na_count_a==3 & df.LFQ$na_count_b==3 & df.LFQ$na_count_c==3 & df.LFQ$na_count_d==3),1:12]
#1573
df.LFQ.filter[is.na(df.LFQ.filter)] <- 0

#subset
# Filter protein table. DEqMS require minimum two values for each group.


## ----pepCountTable---------------------------------------------------------
library(matrixStats)
# we use minimum peptide count among six samples
# count unique+razor peptides used for quantification
# A razor peptide is a peptide that has been assigned to the Protein Group with the largest number of total peptide identified (IDs). 
# If the razor peptide is also unique it only matches to this single Protein Group. 
# If it is not unique, it will only be a razor peptide for the group with the largest number of peptide IDs.

PEP_columns <- grep("Razor", colnames(df.prot)) # get LFQ column numbers
df.PEP= read.csv("Proteomics_ANOVA_Results_R1620.csv", header = T, sep = "\t", row.names = 1)[,PEP_columns]
df.PEP= df.PEP[,c(-1,-3,-4)]
pep.count.table = data.frame(count = rowMins(as.matrix(df.PEP)),
                             row.names = df.prot$Majority.protein.IDs)
# Minimum peptide count of some proteins can be 0
# add pseudocount 1 to all proteins
pep.count.table$count = pep.count.table$count+1

## ----labelfreeDEqMS--------------------------------------------------------
protein.matrix = log2(as.matrix(df.LFQ.filter+1))
colnames(protein.matrix)=c("BC1","BC2","BC3","BD1","BD2","BD3","EC1","EC2","EC3","ED1","ED2","ED3")
class=factor(c(rep("BC",3), rep("BD",3), rep("EC",3), rep("ED",3)))
design = model.matrix(~0+class) # fitting without intercept
colnames(design) = gsub("class","",colnames(design))
# you can define one or multiple contrasts here
x <- c("BD-BC","ED-EC","EC-BC","ED-BD")
contrast =  makeContrasts(contrasts=x,levels=design)
fit1 <- lmFit(protein.matrix, design)
fit2 <- contrasts.fit(fit1,contrasts = contrast)
fit3 <- eBayes(fit2)
fit3$count = pep.count.table[rownames(fit3$coefficients),"count"]
#check the values in the vector fit3$count
#if min(fit3$count) return NA or 0, you should troubleshoot the error first
min(fit3$count)
fit4 = spectraCounteBayes(fit3)

## ----LFQboxplot------------------------------------------------------------
par(mar=c(5,2,5,2))
VarianceBoxplot(fit4, n=20, main = "Label-free dataset BC-BD-EC-ED",
                xlab="peptide count + 1")

## ----LFQresult-------------------------------------------------------------
#if you are not sure which coef_col refers to the specific contrast,type
head(fit4$coefficients)
DEqMS.results1 = outputResult(fit4,coef_col = 1)
DEqMS.results2 = outputResult(fit4,coef_col = 2)
DEqMS.results3 = outputResult(fit4,coef_col = 3)
DEqMS.results4 = outputResult(fit4,coef_col = 4)
results= cbind(DEqMS.results1[,c(1,2,5)],DEqMS.results2[,c(1,2,5)], DEqMS.results3[,c(1,2,5)],DEqMS.results4[,c(1,2,5)]) 

# Add gene names to the data frame
annotation=read.csv("protein2gene.csv", header = T, row.names = 1, sep = "\t")
notes= annotation[unlist(lapply(rownames(results), function(x) grep(x, rownames(annotation), fixed = TRUE))),]
table= cbind(results, fit4$count, notes)
colnames(table)
colnames(table)=c("BDvsBC logFC", "BDvsBC AveExpr", "BDvsBC adj.P.Val",
                  "EDvsEC logFC", "EDvsEC AveExpr", "EDvsEC adj.P.Val",
                  "ECvsBC logFC", "ECvsBC AveExpr", "ECvsBC adj.P.Val",
                  "EDvsBD logFC", "EDvsBD AveExpr", "EDvsBD adj.P.Val",
                  "fit4$count", "Gene.names", "Protein.names","Gene.Ontolgy..GO.")
write.table(table,"DEqMS_results.csv",sep = "\t",row.names = T,quote=F)

deps= table[(table$`BDvsBC logFC`>=4 & abs(table$`BDvsBC adj.P.Val`<=0.05))|
              (table$`EDvsEC logFC`>=4 & abs(table$`EDvsEC adj.P.Val`<=0.05))|
              (table$`ECvsBC logFC`>=4 & abs(table$`ECvsBC adj.P.Val`<=0.05))|
              (table$`EDvsBD logFC`>=4 & abs(table$`EDvsBD adj.P.Val`<=0.05))
            & table$`fit4$count`>=1, c(2,5,8,11)]

library(pheatmap)
pheatmap(deps, main= "log2LFQ", 
         #color = colorRampPalette(c("blue", "black", "yellow"))(75),
         show_rownames = F, 
         cluster_cols = F,
         cluster_rows = T,
         fontsize_row=5, fontsize_col = 8,
         treeheight_row = 50,
         clustering_method = "ward.D",
         clustering_distance_rows= "correlation"
         #kmeans_k = 3
         #gaps_col = 6, 
         #kmeans_k = 2
)

pheatmap(t(scale(t(deps))), main= "log2LFQ", 
         #color = colorRampPalette(c("blue", "black", "yellow"))(75),
         show_rownames = F, 
         cluster_cols = F,
         cluster_rows = T,
         fontsize_row=5, fontsize_col = 8,
         treeheight_row = 50,
         clustering_method = "ward.D",
         clustering_distance_rows= "correlation"
         #kmeans_k = 3
         #gaps_col = 6, 
         #kmeans_k = 2
)

## ----volcanoplot1----------------------------------------------------------
library(ggrepel)
# Use ggplot2 allows more flexibility in plotting

DEqMS.results1$log.sca.pval = -log10(DEqMS.results$sca.P.Value)
ggplot(DEqMS.results1, aes(x = logFC, y =log.sca.pval )) + 
  geom_point(size=0.5 )+
  theme_bw(base_size = 16) + # change theme
  xlab(expression("log2(BD/BC)")) + # x-axis label
  ylab(expression("-log10(P-value)")) + # y-axis label
  geom_vline(xintercept = c(-1,1), colour = "red") + # Add fold change cutoffs
  geom_hline(yintercept = 3, colour = "red") + # Add significance cutoffs
  geom_vline(xintercept = 0, colour = "black") + # Add 0 lines
  scale_colour_gradient(low = "black", high = "black", guide = "none")+
  geom_text_repel(data=subset(DEqMS.results1, abs(logFC)>1&log.sca.pval > 3),
                  aes( logFC, log.sca.pval ,label=gene)) # add gene label

## ----volcanoplot2----------------------------------------------------------
# volcanoplot highlight top 20 proteins ranked by p-value here
volcanoplot(fit4,coef=1, style = "p-value", highlight = 10, 
            names=rownames(fit4$coefficients))
volcanoplot(fit4, coef=2, style = "p-value", highlight = 10, col="red",
            names=rownames(fit4$coefficients))
volcanoplot(fit4,coef=3, style = "p-value", highlight = 10,col="green",
            names=rownames(fit4$coefficients))
volcanoplot(fit4,coef=4, style = "p-value", highlight = 10,col="orange",
            names=rownames(fit4$coefficients))

